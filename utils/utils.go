package utils

import (
	"bytes"
	"compress/gzip"
	"strconv"
	"strings"
	"time"
)

const FILENAME_TEMPLATE = "/chat/{date}/content_logs_{date}_{client_id}"

func GenerateFilename() string {
	filenameWithDate := strings.Replace(FILENAME_TEMPLATE, "{date}", time.Now().Format("2006-01-02"), 2)
	return strings.Replace(filenameWithDate, "{client_id}", strconv.Itoa(5), 2)
}

func GZipData(data []byte) []byte {
	var b bytes.Buffer
	gz := gzip.NewWriter(&b)
	_, err := gz.Write(data)
	if err != nil {
		return nil
	}
	if err = gz.Flush(); err != nil {
		return nil
	}
	if err = gz.Close(); err != nil {
		return nil
	}
	return b.Bytes()
}
