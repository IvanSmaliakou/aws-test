package aws

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
	"os"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"gitlab.com/aws-test/utils"
)

type Session struct {
	session *session.Session
	rs      *ReadSeeker
}

type ReadSeeker struct {
	s []byte
}

func NewAwsSession() (*Session, error) {
	s, err := session.NewSession(&aws.Config{
		Region: aws.String(os.Getenv("AWS_REGION")),
		Credentials: credentials.NewStaticCredentials(
			os.Getenv("ID"),
			os.Getenv("KEY"),
			""),
	})
	if err != nil {
		return nil, err
	}
	return &Session{session: s, rs: NewReadSeeker(nil)}, nil
}

func (s *Session) UploadFile(ch chan []byte) (string, error) {
	var done bool
	sess := s3.New(s.session)
	var c int
	go func() {
		ticker := time.NewTicker(time.Second)
		for {
			select {
			case <-ticker.C:
				fmt.Println("requests per sec: ", c)
				c = 0
			}
		}
	}()
	filename := utils.GenerateFilename()
	for {
		go func() {
			b, ok := <-ch
			if !ok {
				done = true
			}
			s.rs.s = append(s.rs.s, b...)
		}()
		if done {
			return filename, nil
		}
		body := utils.GZipData(s.rs.s)
		bodySize := int64(len(body))
		_, err := sess.PutObject(&s3.PutObjectInput{
			Bucket:               aws.String("myawsomeawsbucket202"),
			Key:                  aws.String(filename),
			ACL:                  aws.String("public-read"),
			Body:                 bytes.NewReader(body),
			ContentLength:        &bodySize,
			ContentType:          aws.String(http.DetectContentType(s.rs.s)),
			ContentDisposition:   aws.String("attachment"),
			ServerSideEncryption: aws.String("AES256"),
			StorageClass:         aws.String("INTELLIGENT_TIERING"),
		})
		if err == io.EOF {
			break
		}
		if err != nil {
			return "", err
		}
		c++
	}
	return filename, nil
}

func NewReadSeeker(b []byte) *ReadSeeker {
	return &ReadSeeker{
		s: b,
	}
}
