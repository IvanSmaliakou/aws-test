package aws

import (
	"errors"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/aws-test/utils"
)

func TestNewAwsSession(t *testing.T) {
	awsSession, err := NewAwsSession()
	assert.NoError(t, err)
	assert.NotNil(t, awsSession)
}

func TestSession_UploadFile(t *testing.T) {
	type out struct {
		filename string
		err      error
	}
	type in struct {
		ch  chan []byte
		arg string
	}
	tests := []struct {
		name     string
		in       *in
		expected *out
	}{
		{
			name:     "success",
			in:       &in{ch: make(chan []byte)},
			expected: &out{filename: utils.GenerateFilename()},
		},
		{
			name:     "err_put",
			in:       &in{ch: make(chan []byte), arg: "123"},
			expected: &out{err: errors.New("err")},
		},
	}
	for _, test := range tests {
		if test.in.arg != "" {
			os.Setenv("ID", test.in.arg)
		}
		s, _ := NewAwsSession()
		go func() {
			test.in.ch <- []byte{byte(1)}
			close(test.in.ch)
		}()
		time.Sleep(time.Second)
		actualFilename, err := s.UploadFile(test.in.ch)
		if test.expected.err == nil {
			assert.NoError(t, err)
		} else {
			assert.Error(t, err)
		}
		assert.Equal(t, test.expected.filename, actualFilename)
	}
}

func BenchmarkSession_UploadFile(b *testing.B) {
	b.ReportAllocs()
	ch := make(chan []byte)
	s, _ := NewAwsSession()
	for i := 0; i < b.N; i++ {
		if _, err := s.UploadFile(ch); err != nil {
			return
		}
	}
}
