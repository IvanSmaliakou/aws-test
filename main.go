package main

import (
	"fmt"
	"log"
	"net/http"
	_ "net/http/pprof"

	"gitlab.com/aws-test/aws"
	"gitlab.com/aws-test/handlers"
)

func main() {
	errChan := make(chan error)
	go func() {
		err := <-errChan
		if err != nil {
			log.Fatal(err)
		}
	}()
	// for profiler usage
	go func() {
		log.Println(http.ListenAndServe("localhost:6060", nil))
	}()
	ch := make(chan []byte, 1000)
	go handlers.HandleRequest(ch, errChan)

	session, err := aws.NewAwsSession()
	if err != nil {
		log.Fatal(err)
	}
	filename, err := session.UploadFile(ch)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Recorded filename: ", filename)
}
