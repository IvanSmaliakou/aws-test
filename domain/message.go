package domain

type Message struct {
	Text      string `json:"text"`
	ContentID string `json:"content_id"`
	ClientID  string `json:"client_id"`
	Timestamp string `json:"timestamp"`
}
