package handlers

import (
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/valyala/fasthttp"
)

// can't test this part
func TestHandleRequest(t *testing.T) {
	errChan := make(chan error)
	ch := make(chan []byte)
	tests := []struct {
		name string
	}{
		{
			name: "success",
		},
	}
	go func() {
		err := fasthttp.ListenAndServe(fmt.Sprintf(":%s", os.Getenv("PORT")), func(ctx *fasthttp.RequestCtx) {
			fmt.Fprint(ctx, "Hello, world}")
		})
		assert.NoError(t, err)
	}()
	for range tests {
		go HandleRequest(ch, errChan)
		assert.Equal(t, []byte{0x48, 0x65, 0x6c, 0x6c, 0x6f, 0x2c, 0x20, 0x77, 0x6f, 0x72, 0x6c, 0x64, 0x7d}, <-ch)
	}
}

func BenchmarkHandleRequest(b *testing.B) {
	errChan := make(chan error)
	ch := make(chan []byte)
	go func() {
		_ = fasthttp.ListenAndServe(fmt.Sprintf(":%s", os.Getenv("PORT")), func(ctx *fasthttp.RequestCtx) {
			fmt.Fprint(ctx, "Hello, world}")
		})
	}()
	for i := 0; i < b.N; i++ {
		go HandleRequest(ch, errChan)
	}
}
