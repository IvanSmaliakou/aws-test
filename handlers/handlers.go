package handlers

import (
	"bufio"
	"fmt"
	"io"
	"net/http"
	"os"
)

func HandleRequest(b chan []byte, errChan chan error) {
	var url = fmt.Sprintf("http://localhost:%s/", os.Getenv("PORT"))
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		errChan <- err
	}
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		errChan <- err
	}
	var bytes []byte
	for {
		bytes, err = bufio.NewReader(resp.Body).ReadBytes('}')
		if err == io.EOF {
			close(b)
			return
		}
		if err != nil {
			errChan <- err
		}
		b <- bytes
	}
}
