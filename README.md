# aws-test

Setup:
- download latest Golang version (https://golang.org/dl/) 
- git clone https://gitlab.com/IvanSmaliakou/aws-test.git
- go get -u ./...
- export ID={your AWS access key ID}
- export KEY={your AWS access key}
- export PORT=8080
- export AWS_REGION={your aws region}

Run:
- go run main.go

Memory usage check:
- go tool pprof http://localhost:6060/debug/pprof/heap
- (in pprof console): top